/* 
 Теоретичні питання
 1. JavaScript цикли - це структури управління, які дозволяють вам повторно виконувати блок коду, поки задана умова є істинною.
 2. Є цикл for (оцінює умову перед ітерацією доки умова не є істиною), 
 while (створює цикл, що оцінює блок коду, доки умова є істиною) & 
 do while ().
 3. While - перевіряє умову перед виконанням блоку коду, якщо умає є неістиною - припиняє роботу і невидає нічого,
 do while - виконує блок коду, а тоді перевіряє умову і він це робить допоку умова є неістиною.

 Практичні завдання
 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. 
 Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. 
 Виведіть на екран всі числа від меншого до більшого за допомогою циклу for. 

 2. Напишіть програму, яка запитує в користувача число та перевіряє, 
 чи воно є парним числом. Якщо введене значення не є парним числом, 
 то запитуйте число доки користувач не введе правильне значення.
*/

// Практичні завдання
//1
let firstNumber = Number(prompt("Please enter you first number"));
while (isNaN(firstNumber)) {
    console.log(typeof firstNumber,isNaN(firstNumber));
    firstNumber= Number(prompt ("Please enter a number not text!"));
}
console.log("firstNumber", firstNumber)

let secondNumber = Number(prompt("Please enter your second number"));
while (isNaN(secondNumber)) {
    console.log(typeof secondNumber,isNaN(secondNumber));
    secondNumber = Number(prompt ("Please enter a number not text!"));
}
console.log("secondNumber", secondNumber)

let smallerNumber = firstNumber < secondNumber ? firstNumber : secondNumber;
console.log("smallerNumber", smallerNumber);

let biggerNumber = firstNumber > secondNumber ? firstNumber : secondNumber;
console.log("biggerNumber", biggerNumber);

for (let index = smallerNumber; index < biggerNumber; index++) {
    console.log("index",index);  
}


//2 
let userNumber = Number(prompt ("Please enter an even number"));
console.log("userNumber", userNumber);

while (userNumber % 2 !== 0) {
   userNumber = (prompt ("Please make sure that your number is even. Try again!"));
}
console.log(userNumber);
